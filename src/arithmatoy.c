#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

const char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
    lhs = drop_leading_zeros(lhs);
    rhs = drop_leading_zeros(rhs);
    size_t llen = strlen(lhs);
    size_t rlen = strlen(rhs);
    size_t max_len;
    if(llen > rlen)
    {
        max_len = llen;
    }
    else
    {
        max_len = rlen;
    }
    char *r = (char *)malloc(max_len + 2);
    int carry = 0;
    int z = 0;

    if (VERBOSE) {
        fprintf(stderr, "add %u %s %s\n",base,lhs,rhs);
        fprintf(stderr, "add: entering function\n");
    }

    for (int i = 0; i < max_len || carry; i++) {
        int lhs_dig;
        int rhs_dig;
        if(i < llen)
        {
            lhs_dig = get_digit_value(lhs[llen - 1 - i]);
        }
        else
        {
            lhs_dig = 0;
        }
        if(i < rlen)
        {
            rhs_dig = get_digit_value(rhs[rlen - 1 - i]);
        }
        else
        {
            rhs_dig = 0;
        }

        int final = lhs_dig + rhs_dig + carry;
        carry = final / base;
        final = final % base;

        r[z] = to_digit(final);
        
        if (VERBOSE) {
            fprintf(stderr, "add: digit %d digit %d carry %d\n", lhs_dig, rhs_dig, carry);
            fprintf(stderr, "add: result: digit %c carry %d\n", r[z], carry);
        }
        z+=1;
    }

    r[z] = '\0';
    reverse(r);

    if (VERBOSE) {
        fprintf(stderr, "add: final carry %d\n", carry);
        fprintf(stderr, "add: final result %s + %s = %s\n", lhs,rhs,r);
    }

    return (char *)drop_leading_zeros(r);
}


const char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
    lhs = drop_leading_zeros(lhs);
    rhs = drop_leading_zeros(rhs);
    size_t llhs = strlen(lhs);
    size_t lrsh = strlen(rhs);
    char *r = (char *)malloc(llhs + 1);
    int b = 0;
    int z = 0;

    if (VERBOSE) {
        fprintf(stderr, "sub %u %s %s\n",base,lhs,rhs);
        fprintf(stderr, "sub: entering function\n");
    }

    for (int i = 0; i < llhs; i++) {
        int lhs_dig = get_digit_value(lhs[llhs - 1 - i]);
        int rhs_dig;
        if(i < lrsh)
        {
            rhs_dig = get_digit_value(rhs[lrsh - 1 - i]);
        }
        else
        {
            rhs_dig = 0;
        }
        int diff = lhs_dig - rhs_dig - b;
        printf("DIFF : %d\n",diff);
        if (diff < 0) {
            diff += base;
            b = 1;
        } else {
            b = 0;
        }

        r[z] = to_digit(diff);
        if (VERBOSE) {
            fprintf(stderr, "sub: digit %d digit %d carry %d\n", lhs_dig, rhs_dig, b);
            fprintf(stderr, "sub: result: digit %c carry %d\n", r[z], b);
        }
        z++;
    }

    if (b != 0) {
        free(r);
        return NULL;
    }

    r[z] = '\0';
    reverse(r);

    if (VERBOSE) {
        fprintf(stderr, "sub: final carry %d\n", b);
        fprintf(stderr, "sub: final result %s + %s = %s\n", lhs,rhs,r);
    }

    return (char *)drop_leading_zeros(r);
}



const char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
    lhs = drop_leading_zeros(lhs);
    rhs = drop_leading_zeros(rhs);
    size_t llhs = strlen(lhs);
    size_t lrsh = strlen(rhs);
    size_t r_len = llhs + lrsh;
    char *r = (char *)calloc(r_len + 1, sizeof(char));
    memset(r, '0', r_len);

    if (VERBOSE) {
        fprintf(stderr, "mul %u %s %s\n",base,lhs,rhs);
        fprintf(stderr, "mul: entering function\n");
    }

    int carry = 0;
    for (size_t i = 0; i < lrsh; i++) {
        int rhs_dig = get_digit_value(rhs[lrsh - 1 - i]);
        carry = 0;
        for (size_t j = 0; j < llhs || carry; j++) {
            int lhs_dig;
            if(j < llhs)
            {
                lhs_dig = get_digit_value(lhs[llhs - 1 - j]);
            }
            else
            {
                lhs_dig = 0;
            }
            int product = rhs_dig * lhs_dig + get_digit_value(r[r_len - 1 - (i + j)]) + carry;
            carry = product / base;
            r[r_len - 1 - (i + j)] = to_digit(product % base);

           if (VERBOSE) {
                fprintf(stderr, "mul: digit %d digit %d carry %d\n", lhs_dig, rhs_dig, carry);
                fprintf(stderr, "mul: result: digit %c carry %d\n", r[j], carry);
            }
        }
    }

    if (VERBOSE) {
        fprintf(stderr, "mul: final carry %d\n", carry);
        fprintf(stderr, "mul: final result %s + %s = %s\n", lhs,rhs,r);
    }
    return drop_leading_zeros(r);
}


// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  size_t l = strlen(str);
  for (size_t i = 0; i < l / 2; ++i) {
    char tmp = str[i];
    size_t mirror = l - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a r with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}